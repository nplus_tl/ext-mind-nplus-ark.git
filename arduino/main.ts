//% color="#758038" iconWidth=38 iconHeight=28
namespace Ark {
    //% block="小方舟初始化直到成功" blockType="command"
    export function ark_init(parameter: any, block: any) {
        if (Generator.board === 'arduino' || Generator.board === 'arduinonano' ){
            Generator.addInclude('ark', '#include "Ark_arduino.h"');
        }
        else if (Generator.board === 'esp32'){
            Generator.addInclude('ark', '#include "Ark_esp32.h"');
        }        
        Generator.addObject(`ark`, `Ark`, `ark;`);
        Generator.addCode('ark.set_init();')
        //Generator.addSetup(`Serial`, `Serial.begin(115200);`);
        //Generator.addSetup(`wait_power`,`ark.wait_power();`);
    }

    //% block="切换到[MODE]模式" blockType="command"
    //% MODE.shadow="dropdown" MODE.options="MODE"
    export function Mode(parameter: any, block: any) {
        let mode = parameter.MODE.code;
        Generator.addCode(`ark.ark_mode(${mode});`);
    }

    //% block="从中心黑框中学习" blockType="command"
    export function ark_learn(parameter: any, block: any) {
        Generator.addCode(`ark.learn_from_center();`);
    }

    //% block="清除当前学习数据" blockType="command"
    export function clen_learn(parameter: any, block: any) {
        Generator.addCode("ark.clean_data();");
    }

    //% block="是否存在方框?" blockType="boolean"
    export function ai_get_data(parameter: any, block: any) {
        Generator.addCode(`ark.get_ai_data()`);
    }

    //% block="当前ID[ID]是否存在 ?" blockType="boolean"
    //% ID.shadow="number" ID.defl="0"
    export function get_id_data(parameter: any, block: any) {
        let id = parameter.ID.code;
        Generator.addCode(`ark.get_id_exist(${id})`);
    }

    //% block="获取当前ID" blockType="reporter""
    export function ai_get_class_id(parameter: any, block: any) {
        Generator.addCode(`ark.get_class_id()`);

    }

    //% block="获取当前准确度" blockType="reporter""
    export function ai_get_class_value(parameter: any, block: any) {
        Generator.addCode(`ark.get_class_value()`);

    }

    //% block="获取ID[ID]第[CNT]个方框[INDEX]参数" blockType="reporter"
    //% ID.shadow="number" ID.defl="0"
    //% CNT.shadow="number" CNT.defl="0"
    //% INDEX.shadow="dropdown" INDEX.options="INDEX"
    export function ai_get_id_data(parameter: any, block: any) {
        let id = parameter.ID.code;
        let cnt = parameter.CNT.code;
        let index = parameter.INDEX.code;
        Generator.addCode(`ark.get_id_data(${id},${cnt},${index})`);
    }    

    //% block="获取当前ID总数" blockType="reporter""
    export function get_id_amount(parameter: any, block: any) {
        Generator.addCode(`ark.get_id_amount()`);

    }

    //% block="设置特定区域颜色识别X[X] Y[Y] W[W] H[H]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% W.shadow="number" W.defl="0"
    //% H.shadow="number" H.defl="0"
    export function color_recognize_roi(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let w = parameter.W.code;
        let h = parameter.H.code;
        Generator.addCode(`ark.color_recognize_roi(${x},${y},${w},${h});`);
    }

    //% block="相同颜色识别[COLOR_MULTIPLE] 亮度设置阈值[THRL]" blockType="command"
    //% COLOR_MULTIPLE.shadow="dropdown" COLOR_MULTIPLE.options="COLOR_MULTIPLE"
    //% THRL.shadow="number" THRL.defl="0"
    export function color_parameter_set(parameter: any, block: any) {
        let color_multiple = parameter.COLOR_MULTIPLE.code;
        let thrl = parameter.THRL.code;
        Generator.addCode(`ark.color_parameter_set(${color_multiple},${thrl});`);
    }

    //% block="获取方框[INDEX]参数" blockType="reporter"
    //% INDEX.shadow="dropdown" INDEX.options="INDEX"
    export function ai_get_center_coord(parameter: any, block: any) {
        let index = parameter.INDEX.code;
        Generator.addCode(`ark.get_center_coord(${index})`);
    }

    //% block="LCD屏幕旋转[DIREC]" blockType="command"
    //% DIREC.shadow="dropdown" DIREC.options="DIREC"
    export function ai_lcd_rotate_horse(parameter: any, block: any) {
        let direc = parameter.DIREC.code;
        Generator.addCode(`ark.lcd_rotation(${direc});`);
    }

    //% block="小方舟LCD旋转[DIREC]" blockType="command"
    //% DIREC.shadow="dropdown" DIREC.options="DIREC"
    export function ai_lcd_rotate_ark(parameter: any, block: any) {
        let direc = parameter.DIREC.code;
        Generator.addCode(`ark.lcd_sensor_rotation(${direc});`);
    }


    //% block="运行分类器" blockType="command"
    export function ai_knn_model_train(parameter: any, block: any) {
        Generator.addCode(`ark.knn_model_train();`);
    }

    //% block="添加到分类[ID]" blockType="command"
    //% ID.shadow="number" ID.defl="0"
    export function ai_add_sample_img(parameter: any, block: any) {
        let id = parameter.ID.code;
        Generator.addCode(`ark.add_sample_img(${id});`);
    }

    //% block="初始化分类器分类为[CLASS_N]" blockType="command"
    //% CLASS_N.shadow="string" CLASS_N.defl=" "
    export function ai_load_knn_model(parameter: any, block: any) {
        let class_n = parameter.CLASS_N.code;
        Generator.addCode(`ark.load_knn_model(${class_n});`);
    }

    //% block="储存分类器为[MODEL_N]" blockType="command"
    //% MODEL_N.shadow="string" MODEL_N.defl=" "
    export function ai_save_knn_file(parameter: any, block: any) {
        let model_n = parameter.MODEL_N.code;
        Generator.addCode(`ark.save_knn_file(${model_n});`);
    }

    //% block="加载分类器[MODEL_N]分类器为[CLASS_N]" blockType="command"
    //% MODEL_N.shadow="string" MODEL_N.defl=" "
    //% CLASS_N.shadow="string" CLASS_N.defl=" "
    export function ai_load_knn_file(parameter: any, block: any) {
        let model_n = parameter.MODEL_N.code;
        let class_n = parameter.CLASS_N.code;
        Generator.addCode(`ark.load_knn_file(${model_n}, ${class_n});`);
    }

    //% block="拍照存储到TF卡[PICTURE]" blockType="command"
    //% PICTURE.shadow="dropdown" PICTURE.options="PICTURE"
    export function ai_photograph(parameter: any, block: any) {
        let picture = parameter.PICTURE.code;
        Generator.addCode(`ark.picture_capture(${picture});`);
    }

    //% block="录像[TIME]秒存储到TF卡" blockType="command"
    //% TIME.shadow="number" TIME.defl="0"
    export function ai_video(parameter: any, block: any) {
        let time = parameter.TIME.code;
        Generator.addCode(`ark.video_capture(${time});`);
    }

    //% block="小方舟[ARKLED]灯 红[R]绿[G]蓝[B]" blockType="command"
    //% ARKLED.shadow="dropdown" ARKLED.options="ARKLED"
    //% R.shadow="range" R.params.min=0 R.params.max=255 R.defl=0
    //% G.shadow="range" G.params.min=0 G.params.max=255 G.defl=0
    //% B.shadow="range" B.params.min=0 B.params.max=255 B.defl=0
    export function ark_rgb(parameter: any, block: any) {
        let arkled = parameter.ARKLED.code;
        let r = parameter.R.code;
        let g = parameter.G.code;
        let b = parameter.B.code;
        Generator.addCode(`ark.set_rgb(${arkled}, ${r}, ${g}, ${b});`);
    }

    //% block="小方舟播放音符[MUSICTONE]节拍[MUSICTICK]" blockType="command"
    //% MUSICTONE.shadow="dropdown" MUSICTONE.options="MUSICTONE"
    //% MUSICTICK.shadow="dropdown" MUSICTICK.options="MUSICTICK"
    export function ark_music_tone(parameter: any, block: any) {
        let musictick = parameter.MUSICTICK.code;
        let muscitone = parameter.MUSICTONE.code;
        Generator.addCode(`ark.buzzer_piano_ring(${muscitone}, ${musictick});`);
    }

    //% block="LCD显示文本X[X]Y[Y]内容[MESS]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% MESS.shadow="string" MESS.defl="HELLO WORLD!"
    export function ai_lcd_dispchar(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let mess = parameter.MESS.code;
        Generator.addCode(`ark.lcd_display_str(${x},${y}, ${mess});`);
    }

    //% block="从SD卡加载[AI_M]模型[MODEL_N]分类为[CLASS_N]" blockType="command"
    //% AI_M.shadow="dropdown" AI_M.options="AI_M"
    //% MODEL_N.shadow="string" MODEL_N.defl="gesture.kmodel"
    //% CLASS_N.shadow="string" CLASS_N.defl="a,b,c"
    export function ai_sd_model(parameter: any, block: any) {
        let ai_m = parameter.AI_M.code;
        let model_n = parameter.MODEL_N.code;
        let class_n = parameter.CLASS_N.code;
        Generator.addCode(`ark.loading_sd_model(${ai_m}, ${model_n}, ${class_n});`);
    }

    //% block="读取SD卡模型分类数据" blockType="reporter""
    export function ai_get_model_data(parameter: any, block: any) {
        Generator.addCode(`ark.get_model_data();`);
    }

    //% block="语音识别 添加关键词[KEY_WORD] 设置识别阈值[ASR_TRL]开始识别" blockType="command"
    //% ASR_TRL.shadow="number" ASR_TRL.defl="0"
    //% KEY_WORD.shadow="string" KEY_WORD.defl="guan-deng,kai-deng"
    export function ai_asr_start(parameter: any, block: any) {
        let asr_trl = parameter.ASR_TRL.code;
        let key_word = parameter.KEY_WORD.code;
        Generator.addCode(`ark.asr_start(${key_word}, ${asr_trl});`);
    }

    //% block="摄像头切换为[AI_CAMERA]" blockType="command"
    //% AI_CAMERA.shadow="dropdown" AI_CAMERA.options="AI_CAMERA"
    export function AI_CAMERA(parameter:any, block:any){
        let ai_camera = parameter.AI_CAMERA.code;
        Generator.addCode(`ark.change_save_sensor(${ai_camera});`);
    }
}
