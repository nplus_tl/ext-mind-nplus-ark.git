#ifndef Ark_h
#define Ark_h

#include <arduino.h>

class Ark{
  public:
    //void init();
    //int ai_read_data[];
    void set_init();
    //byte cmd_data[];
    void wait_power();
    void ark_mode(int mode);
    void ark_uart_cmd(int cmd, int cmd_data[], String str_buf);
    bool read_data(int add, int data[], String str);
    int get_center_coord(int index);
    bool get_ai_data();
    bool get_id_exist(int read_id);
    void clean_data();
    void learn_from_center();
    int get_rect_coord(int index);
    int get_class_id();
    int get_class_value();
    int get_id_data(int read_id, int num, int index);
    int get_id_amount();
    void color_recognize_roi(int x, int y, int w, int h);
    void add_sample_img(int class_id);
    void knn_model_train();
    void color_parameter_set(int color_multiple,int thrl);
    void loading_sd_model(int model,String model_name,String class_name);
    void load_knn_model(String class_name);
    void save_knn_file(String file_name);
    void load_knn_file(String file_name,String class_name);
    void lcd_rotation(int dir);
    void lcd_sensor_rotation(int dir);
    void lcd_display_color(int r,int g,int b);
    void lcd_clear();
    void camera_set_gain(int gain);
    void picture_capture(int model);
    void video_capture(int t);
    void lcd_display_str(int x,int y, String str_buf);
    void buzzer_piano_ring(int pitch,int ton);
    void set_rgb(int num, int r, int g, int b);
    void wifi_connetion(String ssid, String passwd);
    void add_face(String group_id,String user_id);
    void asr_start(String str_buf,int num);
    void change_save_sensor(int t);
        //char *search_face(String group_id);

    int cmd_data[4] = {0, 0, 0, 0};
    int TEMP[9];
    int ai_data[6];
};

#endif