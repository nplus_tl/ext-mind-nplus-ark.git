#include "Ark_esp32.h"
/*
int cmd_data[4]={0,0,0,0};
int TEMP[9];
int ai_data[6];
*/

  void Ark::set_init()
  {
    for(int i=0;i<4;i++)
      cmd_data[i]=0;
    }

  void Ark::ark_uart_cmd(int cmd, int cmd_data[], String str_buf)
  {
    Serial1.flush();
    int check_sum = 0;
    int cmd_temp[7] = {0xAA, 0x55, cmd};
    for (int i = 3; i < 7; i++)
    {
      cmd_temp[i] = cmd_data[i-3];
     }
  for(int j=0;j<7;j++)
  {
    check_sum = check_sum+cmd_temp[j];
    }
  for(int i = 0;i<7;i++)
      Serial1.write(cmd_temp[i]);
  if(str_buf != NULL)
  {
    
    char* cstr_buf = (char*)malloc(sizeof(char)*(str_buf.length()+1));
    str_buf.toCharArray(cstr_buf,str_buf.length()+1);
    Serial1.write(cstr_buf);
    for(int j = 0;j<(str_buf.length()+1);j++)
    {
      check_sum = check_sum + str_buf[j];
      }
     Serial1.write(byte((check_sum>>8)&0xFF));
     Serial1.write(byte(check_sum&0xFF));
     free(cstr_buf);
    }
}

bool Ark::read_data(int add, int data[], String str)
{
  int cnt_num= 0 ;
  int wait_time = 0;
  bool res = false;
  for(int i = 0;i<6;i++)
  {
    ai_data[i]= -1;
  }
  ark_uart_cmd(add,data,str);
  if(add&0xF0 == 0x20 || add&0x0F > 0x09)
  {
    if(add == 0x5A) cnt_num = 2000;
    else cnt_num = 300;
    }
   while(true)
   {
    if(Serial1.available()>0)
    {
      res=false;
      delay(10);
      for(int i = 0;i<9;i++)
      {
        TEMP[i] = Serial1.read();
        }
      if(TEMP[0] == 0x55 && TEMP[1] == 0xAA)
      {
        if(TEMP[2] == add)
        {
          res = true;
          for(int j=0;j<6;j++)
          {
            ai_data[j] = TEMP[j+3];
            //Serial.println(ai_data[j]);
            }
          }
          else if(add &0x0F > 0x09)
          {
            res = read_data(add,data,str);
            }
        }
        return res;
      }
    else
    {
      wait_time++;          
      delay(10);
      if(wait_time > cnt_num)
      {
        Serial1.println("UART_NO_ACK_ERR");
        break;
        }
      }

    }
  }
  
//等待启动成功
  void Ark::wait_power()
  {
    while (true)
    {
      read_data(0x20, cmd_data, "0");
      delay(500);
      if (Serial1.available() > 0)
      {
        if (Serial1.read() == 0x55 && Serial1.read() == 0xAA)
          break;
      }
    }
  } 
  
void Ark::ark_init()
{
  read_data(0x3C, cmd_data, "0"); 
}
/*---------------------------------*/   
//判断是否有数据
  bool Ark::get_ai_data()
  {
    //set_init();
    return read_data(0x21, cmd_data, "");
}

//获取中心坐标 0:X  1:Y   2:W  3:H
int Ark::get_center_coord(int index)
{
  ai_data[0] = ai_data[0] + (ai_data[2]/2);
  ai_data[1] = ai_data[1] + (ai_data[3]/2);
  return ai_data[index];
  }
  
//获取方框坐标坐标
  int Ark::get_rect_coord(int index)
  {
    ai_data[2] = ai_data[0] + (ai_data[2]);
    ai_data[3] = ai_data[1] + (ai_data[3]);
    return ai_data[index];
  }  

//获取识别ID号
  int Ark::get_class_id()
  {
    return ai_data[4];
  }

//获取置信度
  int Ark::get_class_value()
  {
    return ai_data[5];
  }

//判断某ID是否存在
  bool Ark::get_id_exist(int read_id)
  {
    set_init();
    cmd_data[0] = read_id;
    return read_data(0x20,cmd_data,"");
  }
  
//获取某id的第几个框数据
  int Ark::get_id_data(int read_id, int num,int index)
  {
    set_init();
    cmd_data[0] = read_id;
    cmd_data[1] = num;
    if (read_data(0x20, cmd_data, ""))
      return ai_data[index];
    else
      return NULL;
  }
  
//获取某ID颜色的目标总数
  int Ark::get_id_amount()
  {
    return ai_data[5];
  }

/*---------------------------------*/   
  
//模式切换
  void Ark::ark_mode(int mode)
  {
    //set_init();
    *cmd_data = mode;    
    //Serial.println(sizeof(cmd_data));
    ark_uart_cmd(0x10, cmd_data, "");
}
//清除学习内容
  void Ark::clean_data()
  {
    set_init();
    //Serial.println(sizeof(cmd_data));
    ark_uart_cmd(0x12, cmd_data, "");
  }
  //从中心框中学习
  void Ark::learn_from_center()
  {
    //set_init();
    ark_uart_cmd(0x11, cmd_data, "");
  }

//通用模型添加样本图片
void Ark::add_sample_img(int class_id)
{
  set_init();
  *cmd_data = class_id;
  ark_uart_cmd(0x13,cmd_data,"");
  }

//通用模型训练
  void Ark::knn_model_train()
  {
    for (int i = 0; i < 4; i++)
      cmd_data[i] = 0;
    ark_uart_cmd(0x14, cmd_data, "");
  }

//颜色参数设置，color_multiple：使能多重识别，thr_l：颜色识别亮度阈值
  void Ark::color_parameter_set(int color_multiple, int thrl)
  {
    set_init();
    cmd_data[0] = color_multiple;
    cmd_data[1] = thrl;
    ark_uart_cmd(0x15, cmd_data, "");
  }

//特定区域颜色识别，[x,y,w,h]
  void Ark::color_recognize_roi(int x, int y, int w, int h)
  {
    set_init();
    cmd_data[0] = x;
    cmd_data[1] = y;
    cmd_data[2] = w;
    cmd_data[3] = h;
    ark_uart_cmd(0x16, cmd_data, "");
  }


//#SD卡加载模型 mode: 4-分类模型，5-检测模型
  void Ark::loading_sd_model(int model, String model_name, String class_name)
  {
    set_init();
    class_name = ':' + class_name;
    *cmd_data = model;
    if (model_name.endsWith(".kmodel"))
      cmd_data[1] = 1;
    else
      cmd_data[1] = 0;
    model_name.replace(".kmodel", class_name);
    ark_uart_cmd(0x1A, cmd_data, model_name);
  }

//加载通用模型
  void Ark::load_knn_model(String class_name)
  {
    int count = 0;
    int flag = 0;
    set_init();
    while (true)
    {
      flag = class_name.indexOf(',', flag);
      count++;
      Serial1.println(count);
      if (flag > 0)
      {
        flag++;
      }
     else
      break;
    }
    
   cmd_data[1] = count;
   read_data(0x1B,cmd_data,class_name);
  }

//储存通用模型学习文件
  void Ark::save_knn_file(String file_name)
  {
    set_init();
    *cmd_data = 1;
    read_data(0x1B, cmd_data, file_name);
  }

//加载通用模型学习文件
  void Ark::load_knn_file(String file_name, String class_name)
  {
    int count = 0;
    int flag = 0;
    set_init();
    while (true)
    {
      flag = class_name.indexOf(',', flag);
      count++;
      if (flag > 0)
      {
        flag++;
      }
     else
      break;
    }
   cmd_data[0] = 2;
   cmd_data[1] = count;
   read_data(0x1B,cmd_data,(file_name+':'+class_name));
  }

/*---------------------------------*/   

//LCD屏幕旋转
  void Ark::lcd_rotation(int dir)
  {
    set_init();
    *cmd_data = dir;
    ark_uart_cmd(0x30, cmd_data, "");
  }

//LCD&摄像头同时旋转
  void Ark::lcd_sensor_rotation(int dir)
  {
    set_init();
    *cmd_data = dir;
    ark_uart_cmd(0x31, cmd_data, "");
  }

//lcd字符颜色
  void Ark::lcd_display_color(int r, int g, int b)
  {
    set_init();
    cmd_data[0] = r;
    cmd_data[1] = g;
    cmd_data[2] = b;
    ark_uart_cmd(0x32, cmd_data, "");
  }

//清除lcd显示
  void Ark::lcd_clear()
  {
    set_init();
    ark_uart_cmd(0x33, cmd_data, "");
  }

//设置摄像头亮度增益
  void Ark::camera_set_gain(int gain)
  {
    set_init();
    *cmd_data = gain;
    ark_uart_cmd(0x36, cmd_data, "");
  }

 //拍摄图片
  void Ark::picture_capture(int model)
  {
    set_init();
    *cmd_data = model;
    read_data(0x3A, cmd_data, "");
  }

//录制视频
  void Ark::video_capture(int t)
  {
    set_init();
    *cmd_data = t;
    read_data(0x3B, cmd_data, "");
  }

//LCD字符串显示
  void Ark::lcd_display_str(int x, int y, String str_buf)
  {
    set_init();
    cmd_data[0] = x;
    cmd_data[1] = y;
    read_data(0x3C, cmd_data, str_buf); 
    //delay(100); 
    //read_data(0x3C, cmd_data, str_buf); 
  }

/*---------------------------------*/   

//buzze
  void Ark::buzzer_piano_ring(int pitch, int ton)
  {
    set_init();
    cmd_data[0] = pitch;
    cmd_data[1] = ton;
    ark_uart_cmd(0x45, cmd_data, "");  
  }

//RGB
  void Ark::set_rgb(int num, int r, int g, int b)
  {
    set_init();
    cmd_data[0] = num;
    cmd_data[1] = r;
    cmd_data[2] = g;
    cmd_data[3] = b;
    ark_uart_cmd(0x46, cmd_data, "");  
  }

  void Ark::asr_start(String asar_buf, int num)
  {
    set_init();
    cmd_data[0] = num;
    read_data(0x1C, cmd_data, asar_buf);
    delay(100);
    read_data(0x1C, cmd_data, asar_buf);
    //Serial.println(str_buf);
  }

  void Ark::change_save_sensor(int t)
  {
    set_init();
    *cmd_data = t;
    read_data(0x34, cmd_data, "");
  }

 /*---------------------------------*/   

  /*
//在人脸库group_id中搜索人脸
  char *Ark::search_face(String group_id)
  {
    set_init();
    String user_buf;
    if (read_data(0x5C, cmd_data, group_id))
    {
      while (Serial.available() > 0)
      {
        user_buf = user_buf + char(Serial.read());
      }
    char* user = (char*)malloc(sizeof(char)*(user_buf.length()+1));
    user_buf.toCharArray(user,user_buf.length()+1);
    return user;
    }
  }*/