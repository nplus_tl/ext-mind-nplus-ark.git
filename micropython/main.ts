//% color="#758038" iconWidth=38 iconHeight=28
namespace Ark{
    //% block="小方舟初始化" blockType="command"
    //% MODE.shadow="dropdown" MODE.options="MODE"
    export function ark_init(parameter: any,block:any){
        Generator.addImport("from mpython import *");
        Generator.addImport("from nplus_ai import *");
        Generator.addImport("ai = NPLUS_AI()");
    }

    //% block="切换到[MODE]模式" blockType="command"
    //% MODE.shadow="dropdown" MODE.options="MODE"
    export function Mode(parameter:any, block:any){
        let mode = parameter.MODE.code;
        Generator.addCode(`ai.mode_change(${mode})`);
    }

    //% block="从中心方框中学习" blockType="command"
    export function ark_learn(parameter: any, block: any) {
        Generator.addCode(`ai.learn_from_center()`);
    }

    //% block="清除学习数据" blockType="command"
    export function clen_learn(parameter: any, block: any) {
        Generator.addCode("ai.clean_data()");
    }

    //% block="是否存在ID[ID]" blockType="boolean"
    //% ID.shadow="number" ID.defl="0"
    export function ai_get_learn_data(parameter: any, block: any) {
        let id = parameter.ID.code;
        Generator.addCode(`ai.get_id_data(${id})`);
    }

    //% block="方框是否存在数据" blockType="boolean"
    export function ai_get_data(parameter: any, block: any) {
        Generator.addCode(`ai.get_ai_data()`);
    }

    //% block="获取当前分类ID" blockType="reporter""
    export function ai_get_class_id(parameter: any, block: any) {
        Generator.addCode(`ai.get_class_id()`);

    }

    //% block="获取当前分类准确度" blockType="reporter""
    export function ai_get_class_value(parameter: any, block: any) {
        Generator.addCode(`ai.get_class_value()`);

    }

    //% block="获取方框[INDEX]参数" blockType="reporter"
    //% INDEX.shadow="dropdown" INDEX.options="INDEX"
    export function ai_get_coord(parameter: any, block: any) {
        let index = parameter.INDEX.code;
        Generator.addCode(`Dagou.get_coord(${index})`);
    }

    //% block="获取当前分类名称" blockType="reporter""
    export function ai_get_object_name(parameter: any, block: any) {
        Generator.addCode(`ai.get_object_name()`);

    }

    //% block="[ID]颜色是否存在第[COLOR_NUM]个方框" blockType="boolean"
    //% ID.shadow="number" ID.defl="0"
    //% COLOR_NUM.shadow="number" COLOR_NUM.defl="0"
    export function ai_color_id_data(parameter: any, block: any) {
        let id = parameter.ID.code;
        let color_num = parameter.COLOR_NUM.code;
        Generator.addCode(`ai.color_id_data(${id},${color_num})`);
    }

    //% block="获取[ID]颜色方框总数" blockType="reporter"
    //% ID.shadow="number" ID.defl="0"
    export function ai_get_id_color_amount(parameter: any, block: any) {
        let id = parameter.ID.code;
        Generator.addCode(`ai.get_id_color_amount(${id})`);
    }

    //% block="获取二维码内容" blockType="reporter""
    export function get_scan_qr(parameter: any, block: any) {
        Generator.addCode(`ai.get_QR_str()`);

    }

    //% block="---"
    export function noteSep() {

    }

    //% block="小方舟[ARKLED]灯 红[R]绿[G]蓝[B]" blockType="command"
    //% ARKLED.shadow="dropdown" ARKLED.options="ARKLED"
    //% R.shadow="range" R.params.min=0 R.params.max=255 R.defl=0
    //% G.shadow="range" G.params.min=0 G.params.max=255 G.defl=0
    //% B.shadow="range" B.params.min=0 B.params.max=255 B.defl=0
    export function ark_rgb(parameter: any, block: any) {
        let arkled =  parameter.ARKLED.code;
        let r = parameter.R.code;
        let g = parameter.G.code;
        let b = parameter.B.code;
        Generator.addCode(`ai.set_ws2812_rgb(${arkled}, ${r}, ${g}, ${b})`);
    }

    //% block="小方舟LCD&摄像头旋转[DIREC]" blockType="command"
    //% DIREC.shadow="dropdown" DIREC.options="DIREC"
    export function ai_lcd_rotate_ark(parameter: any, block: any) {
        let direc = parameter.DIREC.code;
        Generator.addCode(`ai.lcd_senser_rotation(${direc})`);
    } 

    //% block="LCD显示文本X[X]Y[Y]内容[MESS]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% MESS.shadow="string" MESS.defl="HELLO WORLD!"
    export function ai_lcd_dispchar(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let mess = parameter.MESS.code;
        Generator.addCode(`ai.lcd_display_str(${x},${y}, ${mess})`);
    } 

    //% block="LCD清屏" blockType="command"
    export function ai_lcd_clear(parameter: any, block: any) {
        Generator.addCode(`ai.lcd_display_clear()`);
    }
    //% block="---"
    export function noteSep3() {

    }

    //% block="语音识别 添加关键词[KEY_WORD] 编号为[ASR_NUM]" blockType="command"
    //% KEY_WORD.shadow="string" KEY_WORD.defl="guan-deng"
    //% ASR_NUM.shadow="number" ASR_NUM.defl="0"
    export function ai_asr_addCommand(parameter: any, block: any) {
        let key_word = parameter.KEY_WORD.code;
        let asr_num = parameter.ASR_NUM.code;
        Generator.addCode(`ai.addCommand(${key_word},${asr_num})`);
    }

    //% block="语音识别 设置识别阈值[ASR_TRL]开始识别" blockType="command"
    //% ASR_TRL.shadow="number" ASR_TRL.defl="0"
    export function ai_asr_start(parameter: any, block: any) {
        let asr_trl = parameter.ASR_TRL.code;
        Generator.addCode(`ai.asr_start(${asr_trl})`);
    }

    //% block="---"
    export function noteSep1() {

    }

    //% block="运行分类器" blockType="command"
    export function ai_knn_model_train(parameter: any, block: any) {
        Generator.addCode(`ai.knn_model_train()`);
    } 

    //% block="添加到分类[ID]" blockType="command"
    //% ID.shadow="number" ID.defl="0"
    export function ai_add_sample_img(parameter: any, block: any) {
        let id = parameter.ID.code;
        Generator.addCode(`ai.add_sample_img(${id})`);
    } 

    //% block="初始化分类器分类为[CLASS_N]" blockType="command"
    //% CLASS_N.shadow="string" CLASS_N.defl=" "
    export function ai_load_knn_model(parameter: any, block: any) {
        let class_n = parameter.CLASS_N.code;
        Generator.addCode(`ai.load_knn_model(${class_n})`);
    } 

    //% block="储存分类器为[MODEL_N]" blockType="command"
    //% MODEL_N.shadow="string" MODEL_N.defl=" "
    export function ai_save_knn_file(parameter: any, block: any) {
        let model_n = parameter.MODEL_N.code;
        Generator.addCode(`ai.save_knn_file(${model_n})`);
    } 

    //% block="加载分类器[MODEL_N]分类器为[CLASS_N]" blockType="command"
    //% MODEL_N.shadow="string" MODEL_N.defl=" "
    //% CLASS_N.shadow="string" CLASS_N.defl=" "
    export function ai_load_knn_file(parameter: any, block: any) {
        let model_n = parameter.MODEL_N.code;
        let class_n = parameter.CLASS_N.code;
        Generator.addCode(`ai.load_knn_file(${model_n}, ${class_n})`);
    } 

    //% block="---"
    export function noteSep2() {

    }

    //% block="小方舟播放音符[MUSICTONE]节拍[MUSICTICK]" blockType="command"
    //% MUSICTONE.shadow="dropdown" MUSICTONE.options="MUSICTONE"
    //% MUSICTICK.shadow="dropdown" MUSICTICK.options="MUSICTICK"
    export function ark_music_tone(parameter: any, block: any) {
        let musictick = parameter.MUSICTICK.code;
        let muscitone = parameter.MUSICTONE.code;
        Generator.addCode(`ai.buzzer_piano_ring(${muscitone}, ${musictick})`);
    }

    //% block="拍照存储到TF卡[PICTURE]" blockType="command"
    //% PICTURE.shadow="dropdown" PICTURE.options="PICTURE"
    export function ai_photograph(parameter: any, block: any) {
        let picture = parameter.PICTURE.code;
        Generator.addCode(`ai.picture_capture(${picture})`);
    }

    //% block="录像[TIME]秒存储到TF卡" blockType="command"
    //% TIME.shadow="number" TIME.defl="0"
    export function ai_video(parameter: any, block: any) {
        let time = parameter.TIME.code;
        Generator.addCode(`ai.video_capture(${time})`);
    }

    //% block="设置特定区域颜色识别X[X] Y[Y] W[W] H[H]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% W.shadow="number" W.defl="0"
    //% H.shadow="number" H.defl="0"
    export function color_recognize_roi(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let w = parameter.W.code;
        let h = parameter.H.code;
        Generator.addCode(`ai.color_recognize_roi([${x},${y},${w},${h}])`);
    }

    //% block="设置摄像头亮度增益[GAIN]" blockType="command"
    //% GAIN.shadow="number" GAIN.defl="10"
    export function camer_gain(parameter: any, block: any) {
        let gain = parameter.GAIN.code;
        Generator.addCode(`ai.camera_set_gain(${gain})`);
    }

    //% block="从SD卡加载[AI_M]模型[MODEL_N]分类为[CLASS_N]" blockType="command"
    //% AI_M.shadow="dropdown" AI_M.options="AI_M"
    //% MODEL_N.shadow="string" MODEL_N.defl="gesture.kmodel"
    //% CLASS_N.shadow="string" CLASS_N.defl="a,b,c"
    export function ai_sd_model(parameter: any, block: any) {
        let ai_m = parameter.AI_M.code;
        let model_n = parameter.MODEL_N.code;
        let class_n = parameter.CLASS_N.code;
        Generator.addCode(`ai.loading_sd_model(${ai_m}, ${model_n}, ${class_n})`);
    }

    //% block="读取SD卡模型分类数据" blockType="reporter""
    export function ai_get_model_data(parameter: any, block: any) {
        Generator.addCode(`ai.get_model_data()`);
    }

    //% block="数据格式化" blockType="command""
    export function ai_ram_format(parameter: any, block: any) {
        Generator.addCode(`ai.ram_format()`);
    }

    //% block="摄像头切换为[AI_CAMERA]" blockType="command"
    //% AI_CAMERA.shadow="dropdown" AI_CAMERA.options="AI_CAMERA"
    export function AI_CAMERA(parameter:any, block:any){
        let ai_camera = parameter.AI_CAMERA.code;
        Generator.addCode(`ai.change_save_sensor(${ai_camera})`);
    }
}
    
